# Build stage
FROM node AS build
WORKDIR /build
COPY . .
RUN npm install && npm run build

# Run stage
FROM node
WORKDIR /dist
EXPOSE 3000
COPY --from=build /build/.output /dist/
ENTRYPOINT ["node", "/dist/server/index.mjs"]