# Nuxt GCP Hello World Example

Simple Hello World Nuxt project to test automatically building and deploying to Google Cloud Run.

## Setup

You will first need to create a Google Cloud account and project.  You will also need to enable the Cloud Build and Cloud Run APIs if you haven't already done so for the project.  Then you need to create a service account, you can find this by searching on in the top of the GCP console.  Once you are creating your service account, give it a name and the following roles:

- Cloud Build Service Agent
- Service Account User
- Cloud Run Admin
- Project Viewer

Once created, click on the account, go to the keys tab, and create a new JSON key.  Then head over to your Gitlab project settings, and under CI/CD we will add new variables.  First, you will add GCP_PROJECT_ID equal to the value on the home screen of your GCP project.  Then, you will create another variable GCP_SERVICE_ACCOUNT and copy in the contents of the JSON file as the value.

After you complete all these steps, you are ready to commit the code and it will auto build and deploy to GCP. You can confirm that the service is found under the Cloud Run tab in GCP and visit it via the URL listed within the service details.
